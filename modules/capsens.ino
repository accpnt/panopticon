#include <CapacitiveSensor.h>

CapacitiveSensor Sensor = CapacitiveSensor(4, 6);
long val;
int pos;
#define led 13

void setup()
{
  Serial.begin(9600);
  pinMode(led, OUTPUT);
}

void loop()
{
  val = Sensor.capacitiveSensor(80);
  Serial.println(val);

  delay(10);
}
